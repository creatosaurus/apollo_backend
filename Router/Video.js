const express = require('express')
const router = express.Router()
const Video = require('../Schema/Videoinfo')
const path = require('path')
const fs = require('fs')

router.get('/:id', async (req, res) => {
    try {
        let data = await Video.findById(req.params.id)
        const path1 = path.join(__dirname, `${data.path}`)
        const stat = fs.statSync(path1)
        const fileSize = stat.size
        const range = req.headers.range
        if (range) {
            const parts = range.replace(/bytes=/, "").split("-")
            const start = parseInt(parts[0], 10)
            const end = parts[1]
                ? parseInt(parts[1], 10)
                : fileSize - 1
            const chunksize = (end - start) + 1
            const file = fs.createReadStream(path1, { start, end })
            const head = {
                'Content-Range': `bytes ${start}-${end}/${fileSize}`,
                'Accept-Ranges': 'bytes',
                'Content-Length': chunksize,
                'Content-Type': 'video/mp4',
            }
            res.writeHead(206, head);
            file.pipe(res);
        } else {
            const head = {
                'Content-Length': fileSize,
                'Content-Type': 'video/mp4',
            }
            res.writeHead(200, head)
            fs.createReadStream(path1).pipe(res)
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})


router.get('/download/:id',async (req, res)=>{
    try {
        let data = await Video.findById(req.params.id)
        const path1 = path.join(__dirname, `${data.path}`)
        res.download(path1)
    } catch (error) {
        res.status(500).json({
            message:error
        })
    }
})

router.get('/all/videos',async (req, res)=>{
    try {
        let data = await Video.find({})
        res.send(data)
    } catch (error) {
        res.status(500).json({
            message:error
        })
    }
})

module.exports = router