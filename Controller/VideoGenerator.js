var ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
var ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
const fs = require('fs')
const puppeteer = require('puppeteer');
const Video = require('../Schema/Videoinfo')
const { exec } = require('child_process')
const mongoose = require('mongoose')

const generateTheVideo = async (req, res) => {
    try {
        let duration = 0
        let arrayOfSeen = req.body.arrayOfSeen //the template data to render
        let videoName = mongoose.Types.ObjectId()
        let videoObject = {
            _id: videoName,
            user_id: req.params.id,
            url: `https://apollo.yogved.in/apollo/video/${videoName}`,
            path: `../Videos/${videoName}.mp4`,
            status: 'pending',
            template: JSON.stringify(arrayOfSeen)
        }

        await Video.create(videoObject)
        res.send("done")

        req.body.arrayOfSeen.map(data => {
            duration = parseInt(data.duration) + duration
        })

        let time = secondsToHms(duration)
        function secondsToHms(millis) {
            if (millis % 1000 === 0) {
                var minutes = Math.floor(millis / 60000);
                var seconds = ((millis % 60000) / 1000).toFixed(0);
                return "00" + ":" + (minutes < 10 ? '0' : '') + minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
            } else {
                var minutes = Math.floor(millis / 60000);
                var seconds = ((millis % 60000) / 1000).toFixed(0) - 1
                let millisecondes = millis % 1000
                return "00" + ":" + (minutes < 10 ? '0' : '') + minutes + ":" + (seconds < 10 ? '0' : '') + seconds + "." + millisecondes;
            }
        }

        arrayOfSeen.map((data) => {
            data.data.map((data1) => {
                if (data1.type === "audio") {
                    exec(`ffmpeg -i ${data1.fileName} -ss 00:00:00 -to ${time} -c copy ${videoName}.mp3`, (error, stdout, stderr) => {
                        if (error) {
                            console.log(`error: ${error.message}`);
                            return;
                        }
                        if (stderr) {
                            console.log(`stderr: ${stderr}`);
                            return;
                        }
                        console.log("done")
                    });
                }
            })
        })

        /*fs.writeFileSync(`seen${videoName}.txt`)
        fs.openSync(`seen${videoName}.txt`, 'w')
        let i4 = 0
        arrayOfSeen.map((data) => {
            data.data.map((data1) => {
                if (data1.type === "audio") {
                    i4 = i4 + 1
                    fs.appendFileSync(`seen${videoName}.txt`, `file output${i4}.mp3 \n`)
                    if (!isNaN(data1.startTime) && !isNaN(data1.endTime) && !isNaN(data1.volume)) {
                        exec(`ffmpeg -i ${data1.fileName} -ss 00:00:00 -to ${time} -c copy output${i4}.mp3`, (error, stdout, stderr) => {
                            if (error) {
                                console.log(`error: ${error.message}`);
                                return;
                            }
                            if (stderr) {
                                console.log(`stderr: ${stderr}`);
                                return;
                            }
                            console.log("done")
                        });
                    }
                }
            })
        })


        setTimeout(() => {

            exec(`ffmpeg -f concat -i seen${videoName}.txt -c copy ${videoName}.mp3`, (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    return;
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`);
                    return;
                }
                console.log("done")
            });

        }, 5000);



        //-ss 00:00:20 -to 00:00:40 trim audio
        //ffmpeg -f concat -i mylist.txt -c copy out.mp3  //concat audio
        //
        /* for (let index = 1; index <= 2; index++) {
             fs.appendFileSync('mylist.txt', `file output${index}.mp3 \n`)
             exec(`ffmpeg -i music${index}.mp3 -ss 00:00:00 -to 00:00:10 -filter:a 'volume=0.1'  output${index}.mp3`, (error, stdout, stderr) => {
                 if (error) {
                     console.log(`error: ${error.message}`);
                     return;
                 }
                 if (stderr) {
                     console.log(`stderr: ${stderr}`);
                     return;
                 }
                 console.log("done")
             });
         }
 
         setTimeout(() => {
             exec(`ffmpeg -f concat -i mylist.txt -c copy out.mp3`, (error, stdout, stderr) => {
                 if (error) {
                     console.log(`error: ${error.message}`);
                     return;
                 }
                 if (stderr) {
                     console.log(`stderr: ${stderr}`);
                     return;
                 }
                 console.log("done")
             });
         }, 5000); */


        //play with the request from user
        const height = parseInt(req.body.canvasDimension.height)
        const width = parseInt(req.body.canvasDimension.width)
        let multiplyer = 3
        if (width === 270 && height === 480) {
            multiplyer = 4
        } else {
            multiplyer = 3
        }

        //this is set to give the name to the images
        let i = 0

        // create the directory and the txt file temporary
        fs.mkdirSync(`./${videoName}`);
        fs.writeFileSync(`${videoName}.txt`)
        fs.openSync(`${videoName}.txt`, 'w')


        const getTheImageAndStoreToTheFile = (base) => {
            var base64Data = base.replace(/^data:image\/jpeg;base64,/, "");
            fs.appendFileSync(`${videoName}.txt`, `file ./${videoName}/image${i}.jpeg \n`)
            fs.writeFile(`./${videoName}/image${i}.jpeg`, base64Data, 'base64', function (err) {
            });
            i++
        }

        // set up the browser
        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox'],
            //executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
            executablePath:'/usr/bin/google-chrome-stable'
        });

        //open new page in the browser
        const page = await browser.newPage();
        page.setViewport({ width: width * multiplyer, height: height * multiplyer });
        await page.exposeFunction("getTheImageAndStoreToTheFile", getTheImageAndStoreToTheFile); // call the external function from page evaluation


        // the script for the creating canvas and rendering template
        await page.evaluate(async (arrayOfSeen, width, height, multiplyer) => {
            // Global variables
            let flag = 0
            let i2 = 0
            let data3
            let seen = 0
            let endTime = 0

            // create the canvas as per the template
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');
            canvas.width = width * multiplyer;
            canvas.height = height * multiplyer;

            const createElementToDom = (data) => {
                data.map(data => {

                    // defining the animation
                    if (data.animation) {
                        if (data.animation.type === "right-to-left") {
                            data.animation = { play: true, x: canvas.width, y: 0, type: "right-to-left", speed: 1 * 6 }
                        } else if (data.animation.type === "left-to-right") {
                            data.animation = { play: true, x: 0, y: 0, type: "left-to-right", speed: 1 * 6 }
                        } else if (data.animation.type === "top-to-bottom") {
                            data.animation = { play: true, x: 0, y: 0, type: "top-to-bottom", speed: 1 * 6 }
                        } else if (data.animation.type === "bottom-to-top") {
                            data.animation = { play: true, x: 0, y: canvas.height, type: "bottom-to-top", speed: 1 * 6 }
                        }
                    }

                    //creating the elements 
                    if (data.type === "image") {
                        var x = document.createElement("img");
                        x.setAttribute("id", data.elementId);
                        x.src = data.src;
                        x.crossOrigin = 'anonymous'
                        x.setAttribute("width", "320");
                        x.setAttribute("height", "240");
                        document.body.appendChild(x);
                    } else if (data.type === "video") {
                        var x = document.createElement("VIDEO");
                        x.setAttribute("id", data.elementId);
                        if (x.canPlayType("video/mp4")) {
                            x.src = data.src
                        }
                        x.setAttribute("width", "320");
                        x.setAttribute("height", "240");
                        x.setAttribute("controls", "controls");
                        x.autoplay = false;
                        x.preload = "auto"
                        x.crossOrigin = 'anonymous'
                        document.body.appendChild(x);
                    }
                })
            }

            arrayOfSeen.map(data => {
                endTime = endTime + parseInt(data.duration)
                createElementToDom(data.data)
            })
            let calculateEndTime = endTime / 33.33
            endTime = calculateEndTime * 200
            endTime = endTime + 5000 // the time require to end the all process and wait extra for 5 sec

            //Animation Section Started

            // Text Animation started
            const moveTheAnimationLeftToRightText = (text) => {
                ctx.globalAlpha = text.opacity
                ctx.font = text.size * multiplyer + "px Arial";
                ctx.fillStyle = text.color;
                if (text.x * multiplyer > text.animation.x) {
                    ctx.fillText("Hello World", text.animation.x = text.animation.x + text.animation.speed, text.y * multiplyer);
                } else {
                    ctx.fillText("Hello World", text.x * multiplyer, text.y * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }

            const moveTheAnimationBottomToTopText = (text) => {
                ctx.globalAlpha = text.opacity
                ctx.font = text.size * multiplyer + "px Arial";
                ctx.fillStyle = text.color;
                if (text.y * multiplyer < text.animation.y) {
                    ctx.fillText("Hello World", text.x * multiplyer, text.animation.y = text.animation.y - text.animation.speed);
                } else {
                    ctx.fillText("Hello World", text.x * multiplyer, text.y * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }

            const moveTheAnimationRightToLeftText = (text) => {
                ctx.globalAlpha = text.opacity
                ctx.font = text.size * multiplyer + "px Arial";
                ctx.fillStyle = text.color;
                if (text.x * multiplyer < text.animation.x) {
                    ctx.fillText("Hello World", text.animation.x = text.animation.x - text.animation.speed, text.y * multiplyer);
                } else {
                    ctx.fillText("Hello World", text.x * multiplyer, text.y * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }

            const moveTheAnimationTopToBottomText = (text) => {
                ctx.globalAlpha = text.opacity
                ctx.font = text.size * multiplyer + "px Arial";
                ctx.fillStyle = text.color;
                if (y * multiplyer > text.animation.y) {
                    ctx.font = text.size * multiplyer + "px Arial";
                    ctx.fillStyle = text.color;
                    ctx.fillText("Hello World", text.x * multiplyer, text.animation.y = text.animation.y + text.animation.speed);
                } else {
                    ctx.fillText("Hello World", text.x * multiplyer, text.y * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }
            // Text Animation ended

            // Image Animation started 
            const moveTheAnimationLeftToRightImage = (image) => {
                var img = document.getElementById(image.elementId);
                ctx.globalAlpha = image.opacity
                if (image.x * multiplyer > image.animation.x) {
                    ctx.drawImage(img, image.animation.x = image.animation.x + image.animation.speed, image.y * multiplyer, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                } else {
                    ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }

            const moveTheAnimationRightToLeftImage = (image) => {
                var img = document.getElementById(image.elementId);
                ctx.globalAlpha = image.opacity
                if (image.x * multiplyer < image.animation.x) {
                    ctx.drawImage(img, image.animation.x = image.animation.x - image.animation.speed, image.y * multiplyer, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                } else {
                    ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }

            const moveTheAnimationTopToBottomImage = (image) => {
                var img = document.getElementById(image.elementId);
                ctx.globalAlpha = image.opacity
                if (image.y * multiplyer > image.animation.y) {
                    ctx.drawImage(img, image.x * multiplyer, image.animation.y = image.animation.y + image.animation.speed, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                } else {
                    ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }

            const moveTheAnimationBottomToTopImage = (image) => {
                var img = document.getElementById(image.elementId);
                ctx.globalAlpha = image.opacity
                if (image.y * multiplyer < image.animation.y) {
                    ctx.drawImage(img, image.x * multiplyer, image.animation.y = image.animation.y - image.animation.speed, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                } else {
                    ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                }
                ctx.globalAlpha = 1.0
            }
            // Image Animation ended

            //Animation Section Started

            await new Promise(resolve => setTimeout(resolve, 10000)); // wait for the element to load in the canvas

            // Drawing Functiones of the canvas 
            const previewDrawRectangleOnCanvas = (rectangle) => {
                ctx.globalAlpha = rectangle.opacity
                ctx.beginPath();
                ctx.fillStyle = rectangle.color;
                ctx.rect(rectangle.x * multiplyer, rectangle.y * multiplyer, rectangle.width * multiplyer, rectangle.height * multiplyer);
                ctx.fill();
                ctx.globalAlpha = 1.0
            }

            const previewDrawImageOnCanvas = (image) => {
                if (image.animation !== null) {
                    if (image.animation.type === "left-to-right") {
                        moveTheAnimationLeftToRightImage(image)
                    } else if (image.animation.type === "right-to-left") {
                        moveTheAnimationRightToLeftImage(image)
                    } else if (image.animation.type === "top-to-bottom") {
                        moveTheAnimationTopToBottomImage(image)
                    } else if (image.animation.type === "bottom-to-top") {
                        moveTheAnimationBottomToTopImage(image)
                    }
                } else {
                    ctx.globalAlpha = image.opacity
                    var img = document.getElementById(image.elementId);
                    ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width / 3 * multiplyer, image.height / 3 * multiplyer);
                    ctx.globalAlpha = 1.0
                }
            }

            const previewDrawVideoOnCanvas = (data) => {
                var video = document.getElementById(data.elementId);
                ctx.globalAlpha = data.opacity
                ctx.drawImage(video, data.x * multiplyer, data.y * multiplyer, data.width / 3 * multiplyer, data.height / 3 * multiplyer);
                ctx.globalAlpha = 1.0
                video.currentTime += 1 / 30;
            }

            const previewDrawTextOnCanvas = (text) => {
                if (text.animation !== null) {
                    if (text.animation.type === "left-to-right") {
                        moveTheAnimationLeftToRightText(text)
                    } else if (text.animation.type === "right-to-left") {
                        moveTheAnimationRightToLeftText(text)
                    } else if (text.animation.type === "top-to-bottom") {
                        moveTheAnimationTopToBottomText(text)
                    } else if (text.animation.type === "bottom-to-top") {
                        moveTheAnimationBottomToTopText(text)
                    } else if (text.animation.type === "zoom") {
                        zoomText(text)
                    }
                } else {
                    ctx.globalAlpha = text.opacity
                    ctx.font = text.size * multiplyer + "px Arial";
                    ctx.fillStyle = text.color;
                    ctx.fillText("Hello World", text.x * multiplyer, text.y * multiplyer);
                    ctx.globalAlpha = 1.0
                }
            }
            // Drawing Functiones of the canvas end

            //@desc takes the image of the canvas by 200 millisecondes and draw it according to the 30fps 
            const getImagesFromCanvas = () => {
                if (flag === 0) {
                    flag = 1
                    const fetchImages = async () => {
                        i2 = i2 + 33.33
                        if (i2 <= parseInt(arrayOfSeen[seen].duration)) {
                            let data = canvas.toDataURL('image/jpeg', 0.80)
                            await getTheImageAndStoreToTheFile(data);
                            Preview(arrayOfSeen[seen].data, arrayOfSeen[seen].backgroundColor)
                        } else {
                            clearInterval(data3)
                            flag = 0
                            i2 = 0
                            seen++
                            if (arrayOfSeen.length - 1 < seen) return
                            getImagesFromCanvas()
                        }
                    }
                    data3 = setInterval(fetchImages, 200);
                }
            }

            //@desc call the draw methodes of the canvas to render
            const Preview = (data1, backgroundColor) => {

                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.fillStyle = backgroundColor;
                ctx.fillRect(0, 0, canvas.width, canvas.height);

                data1.map(data => {
                    if (data.type === "rectangle") {
                        previewDrawRectangleOnCanvas(data)
                    } else if (data.type === "image") {
                        previewDrawImageOnCanvas(data)
                    } else if (data.type === "video") {
                        previewDrawVideoOnCanvas(data)
                    } else if (data.type === "text") {
                        previewDrawTextOnCanvas(data)
                    }
                })
                getImagesFromCanvas() // call the ones so the image fetching process can be start
            }

            //@desc take the first seen and draw
            Preview(arrayOfSeen[0].data, arrayOfSeen[0].backgroundColor)
            await new Promise(resolve => setTimeout(resolve, endTime)); // wait till the image processing finish
            return
        }, arrayOfSeen, width, height, multiplyer)

        await browser.close() // close the browser once rendering completed

        var proc = new ffmpeg();

        proc.addInput(`${videoName}.txt`)
            .on('start', function (ffmpegCommand) {
                /// log something maybe
                console.log(ffmpegCommand)
            })
            .on('progress', function (data) {
                /// do stuff with progress data if you want
                console.log(data)
            })
            .on('end', function () {
                /// encoding is complete, remove the directory which will be not get used
                fs.unlinkSync(`${videoName}.txt`)
                fs.unlinkSync(`${videoName}.mp3`)
                fs.rmdirSync(`${videoName}`, { recursive: true });
                Video.updateOne({ _id: videoName }, { status: 'completed' }).then(data => {
                    console.log(data)
                })
            })
            .on('error', function (error) {
                console.log(error)
                fs.unlinkSync(`${videoName}.mp3`)
                fs.unlinkSync(`${videoName}.txt`)
                fs.rmdirSync(`${videoName}`, { recursive: true });
            })

            //add the multiple sound at a single time
            .withFpsInput(30) // fps matching your jpg or png input sequence  
            .addInputOption(['-f concat', '-safe 0'])
            .outputOptions(['-c:v libx264', '-r 30', '-pix_fmt yuv420p'])
            //.input('music.mp3')
            //.inputOptions('-t 10')
            .input(`${videoName}.mp3`)
            /*.complexFilter([{
                filter: 'amix', options: { inputs: 2, duration: 'longest' }
            }])*/
            .output(`./Videos/${videoName}.mp4`)
            .run();
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    generateTheVideo: generateTheVideo
}